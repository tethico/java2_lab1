package FS;

public abstract class FSNode {
    public enum FSNodeType {
        DIRECTORY,
        BINARY_FILE,
        LOG_TEXT_FILE,
        BUFFER_FILE,
    }
    protected Directory parent;
    private String name;

    public abstract FSNodeType getFSNodeType();

    public void delete() {
    }

    public String getName() {
    }

    protected FSNode(String name) {
    }

    protected FSNode(String name, Directory dir) {
    }

    public void move(Directory destination) {
    }

    public boolean isDirectory() {
    }
}