package FS;

public class LogTextFile extends FSNode {
    private StringBuffer data;

    private LogTextFile(String name, Directory parent, String data) {
    }

    public static LogTextFile create(String name, Directory parent, String data) {
    }

    public String read() {
    }

    public void appendLine(String line) {
    }

    @Override
    public FSNodeType getFSNodeType() {
        return FSNodeType.LOG_TEXT_FILE;
    }
}
