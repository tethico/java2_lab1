package FS;

import java.util.HashMap;
import java.util.Map;

public class Directory extends FSNode {
    private String DirName;

    @Override
    public FSNodeType getFSNodeType() {
        return FSNodeType.DIRECTORY;
    }

    protected Directory(String name) {
    }

    protected Directory(String name, Directory directory) {
    }

    public boolean contains(String name) {
    }

    public boolean contains(FSNode node) {
    }

    public void deleteNode(FSNode node) {
    }

    public void deleteNode(String nodeName) {
    }

    public boolean addNode(FSNode node) {
    }
}
