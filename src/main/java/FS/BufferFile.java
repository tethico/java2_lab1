package FS;

import java.util.Stack;

public class BufferFile<T> extends FSNode {
    private Stack<T> stack;

    private BufferFile(String name, Directory parent) {
    }

    public static <T> BufferFile<T> create(String name, Directory parent) {
    }

    public void push(T element) {
    }

    public T pop() {
    }

    @Override
    public FSNodeType getFSNodeType() {
        return FSNodeType.BUFFER_FILE;
    }
}
