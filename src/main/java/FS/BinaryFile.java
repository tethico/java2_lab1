package FS;

public class BinaryFile extends FSNode {
    private byte[] data;

    private BinaryFile(String name, Directory parent, byte[] data) {

    }

    public static BinaryFile create(String name, Directory parent, byte[] data) {

    }

    public byte[] read() {

    }

    @Override
    public FSNodeType getFSNodeType() {
        return FSNodeType.BINARY_FILE;
    }
}
